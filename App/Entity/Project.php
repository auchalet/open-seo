<?php

namespace App\Entity;

class Project
{
	/**
	 * @var integer
	 */
	private $id;
    
	/**
	 * @var integer
	 */
	private $name;
    
	/**
	 * @var integer
	 */
	private $user;
    
	/**
	 * @var integer
	 */
	private $host;
    
    function __construct($user) {
        $this->user = $user;
        
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getUser() {
        return $this->user;
    }

    function getHost() {
        return $this->host;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setUser($user) {
        $this->user = $user;
    }

    function setHost($host) {
        $this->host = $host;
    }


}