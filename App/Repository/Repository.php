<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;

class Repository
{
	protected $db;

	public function __construct(Connection $db)
	{
		$this->db = $db;
	}

	protected function getDb()
	{
		return $this->db;
	}
}