<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use App\Repository\Repository;
use App\Entity\User;

class UserRepository extends Repository
{
    
	public function findAllUser()
	{
		$sql = "SELECT * FROM user";
		$result = $this->getDb()->fetchAll($sql);
        
		$entities = array();
		foreach ( $result as $row ) {
			$id = $row['id'];
			$entities[$id] = new User($row);
		}

		return $entities;
	}
    
    public function testConnection($pass, $email) {
        $sql = 'SELECT * FROM user WHERE email = :email AND pass = :pass';
		$result = $this->getDb()->prepare($sql);
        $result->bindValue(':email', $email);
        $result->bindValue(':pass', $pass);
        $result->execute();
        
        $count = $result->rowCount();
        if($count == 1){
            $id_user = $result->fetch();
            return $id_user['id'];
        } else {
            return false;
        }
    }
    
    public function getInfoConnection($user) {
        $sql = 'SELECT * FROM projects WHERE user = :user ORDER BY name';
		$result = $this->getDb()->prepare($sql);
        $result->bindValue(':user', $user);
        $result->execute();
        //$result->fetch();
    }
    
    public function deleteUser($user_id){
        // suppression
        return false;
    }
    
    public function createUser($user){
        //creatio user
        return true;
    }

}