<?php
namespace App\Controller {

    use Silex\Application;
    use Silex\ControllerProviderInterface;
    use App\Entity\Project;
    use App\Repository\ProjectRepository;
    
    class ProjectController 
    {
        protected $projectRepository;

//        public function connect(Application $app) {
//            $index = $app['controllers_factory'];
//            
//            $index->match("/", 'App\Controller\ProjectController::listAll');
//            $index->match("/create/", 'App\Controller\ProjectController::create');
//            
//            return $index;
//        }
        
        public function listAction(Application $app) {
            $this->initRepository($app);
            
            $userList = $this->userRepository->findAllUser();
            
            return $app["twig"]->render("project/list-all.twig", array(
                'users' => $userList,
            ));
        }
        
        public function createAction(Application $app) {
            return $app["twig"]->render("project/create.twig");
        }
        
        public function deleteUserAction($user_id) {
            $this->initRepository($app);
                        
            $this->userRepository->deleteUser($user_id);
        }
        
        private function initRepository(Application $app) {
            $this->userRepository = new ProjectRepository($app['db']);
        }
        
    }
}