<?php

namespace App\Controller;

use Silex\Application;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class UserController {


    public function loginAction(Application $app, Request $request)
    {
        $user = new User;
        
        $form = $app['form.factory']->createBuilder(FormType::class, $user)
                ->add('username')
                ->add('email')
                ->add('password', PasswordType::class)  
                ->add('connect', SubmitType::class, [
                    'label' => 'Connexion',
                    'attr' => [
                        'class' => 'btn btn-success'
                    ]
                ])
                ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isValid()) {
            $data = $form->getData();
            var_dump($data);die;
        }
        
        return $app['twig']->render('user/login.html.twig', [
            'form' => $form->createView()
        ]);
        
    }
    
    public function logoutAction()
    {
        
    }
    
    public function registerAction(Application $app, Request $request)
    {
        $user = new User;
        
        $form = $app['form.factory']->createBuilder(FormType::class, $user)
                ->add('username')
                ->add('email', EmailType::class)
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'Password',
                    ],
                    'second_options' => [
                        'label' => 'Confirm Password'
                    ]
                ])
                ->add('register', SubmitType::class, [
                    'label' => 'Inscription',
                    'attr' => [
                        'class' => 'btn btn-success'
                    ]
                ])
                ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isValid()) {
            $data = $form->getData();
            
            $em = $app['orm.em'];
            
            $em->persist($data);
            $em->flush();
            
            $app['session']->getFlashBag()->add('message', 'Inscription réussie');
            
            return $app->redirect($app['url_generator']->generate('index'));
            
        }

        return $app['twig']->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
        
    }
    
    public function activateUser()
    {
        
    }
    
    public function viewAction($id)
    {
        var_dump($id);die;
    }
    
    private function encodePass($pass) {
        $pass = $_POST['pass'];
        $pass .= '15MD.?/PPpmé+Uipo+-Uii"ù%aR-[';
        $pass = sha1($pass);

        return $pass;
    }

}
