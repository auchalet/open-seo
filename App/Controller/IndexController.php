<?php

namespace App\Controller;

use Silex\Application;
use Silex\Controller;



use Symfony\Component\HttpFoundation\Request;

use Silex\Provider\FormServiceProvider;


class IndexController {

    
    public function indexAction(Application $app, Request $request) {

        // Si User connecté, rend la vue, sinon redirect vers la page de login
        if($request->getUser() !== NULL) {
            
        } else {
            return $app->redirect($app['url_generator']->generate('user_login'));
        }

    }

}
