<?php

use Silex\Provider;
use Silex\Application;
use Symfony\Component\Yaml\Yaml;


define('APP_DIR', dirname(__DIR__));
define('WEB_DIR', dirname(__DIR__).'/web');
define('CONF_DIR', dirname(__DIR__).'/config');


$app = new Application();


// Mode production par défaut
$app['debug'] = false;


// Yaml
$config = Yaml::parse(file_get_contents(CONF_DIR.'/config.yml'));
$routes = Yaml::parse(file_get_contents(CONF_DIR.'/routes.yml'));
$security = Yaml::parse(file_get_contents(CONF_DIR.'/security.yml'));


// DB
$app->register(new Provider\DoctrineServiceProvider(), array(
    'db.options' => $config['db']['options']
));

// ORM
$app->register(new Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), array(
    'orm.proxies.dir' => $config['orm']['proxies']['dir'],
    'orm.em.options' => [
        'mappings' => [
            $config['orm']['em']['options']['mappings']
        ]
    ]
));

// Security / Authentication
//$app->register(new Provider\SecurityServiceProvider());

// Session
$app->register(new Provider\SessionServiceProvider(), array(
    'security.firewalls' => array(
        'login' => array(
            'pattern' => '^/login$'
        )
    )
));

// Form
$app->register(new Provider\FormServiceProvider);
$app->register(new Provider\ValidatorServiceProvider);
$app->register(new Provider\TranslationServiceProvider, array(
    'translator.domains' => array(),
    'locale' => 'fr_FR'
));

// Views
$app->register(new Provider\HttpFragmentServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => $config['twig']['path'],
    'twig.options' => $config['twig']['options']
));


// Routing
$app->register(new Silex\Provider\RoutingServiceProvider());

function controller($shortName)
{
    list($shortClass, $shortMethod) = explode('/', $shortName, 2);

    return sprintf('App\Controller\%sController::%sAction', ucfirst($shortClass), $shortMethod);
}

foreach($routes as $name => $route) {
    
    $app->{(isset($route['method'])) ? $route['method'] : "match"}($route['pattern'], controller($route['defaults']['_controller']))->bind($name);
}




