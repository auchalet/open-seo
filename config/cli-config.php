<?php
    // http://docs.doctrine-project.org/en/latest/reference/configuration.html
    require_once __DIR__.'/../vendor/autoload.php';
    require_once __DIR__.'/../App/app.php';
    
    use Doctrine\Common\Annotations\AnnotationRegistry;
    use Doctrine\Common\Annotations\AnnotationReader;
    

    $ormconfig = new \Doctrine\ORM\Configuration();
    $ormconfig->setMetadataCacheImpl(new \Doctrine\Common\Cache\ApcuCache);
    
    AnnotationRegistry::registerFile(__DIR__. "/../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php");
    $reader = new AnnotationReader();
    
    $newDefaultAnnotationDrivers = array(
        __DIR__."/../App/Entity",
    );
    //$driverImpl = $config->newDefaultAnnotationDriver($newDefaultAnnotationDrivers);
    $driverImpl = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, array(__DIR__ . "/../App/Entity"));
    
    $ormconfig->setMetadataDriverImpl($driverImpl);
    $ormconfig->setProxyDir(__DIR__.'/../cache/doctrine/proxy');
    $ormconfig->setProxyNamespace('Proxies');
    
