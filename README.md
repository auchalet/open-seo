Open-SEO
========
Outil libre et open-source pour gérer votre référencement simplement.
Développé grâce au micro-framework Silex

Doctrine CLI
----------

https://wildlyinaccurate.com/useful-doctrine-2-console-commands/

Création de la base de données à partir d'un fichier :

    php bin/console dbal:import <chemin vers le fichier>
 
Génération des Entities via les tables de la BD :

    php bin/console orm:convert:mapping --from-database --namespace="App\Entity" annotation App/Entity

Création de la base de données :

    php bin/console orm:schema-tool:create

Génération des Entities avec les méthodes getters et setters

    php bin/console orm:generate:entities --generate-annotations="true"
