ENV = "dev"

usage:
	@echo "Usage: make [command]";
	@echo "  cache-clear			Manually clear cache folder";

cache-clear:
	rm -rf cache/*
