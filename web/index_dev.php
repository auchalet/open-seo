<?php

//On ajoute l'autoloader
$loader = require_once __DIR__ . '/../vendor/autoload.php';

use Silex\Provider;


//On initialise le timeZone
ini_set('date.timezone', 'Europe/Brussels');


//dans l'autoloader nous ajoutons notre répertoire applicatif 
$loader->add("App", dirname(__DIR__));

require __DIR__.'/../App/app.php';


//en dev, nous voulons voir les erreurs
$app['debug'] = true;

//Barre de debug -- DEV
if($app['debug'] === true) {
    
    $app->register(new Provider\ServiceControllerServiceProvider());
    $app->register(new Provider\WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => __DIR__.'/../cache/profiler',
        'profiler.mount_prefix' => '/_profiler', // this is the default
    ));
    
    $app->register(new Sorien\Provider\DoctrineProfilerServiceProvider());
    
    

}



//On lance l'application
$app->run();