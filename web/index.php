<?php

//On ajoute l'autoloader
$loader = require_once __DIR__ . '/../vendor/autoload.php';

use Silex\Application;


//On initialise le timeZone
ini_set('date.timezone', 'Europe/Brussels');


//dans l'autoloader nous ajoutons notre répertoire applicatif 
$loader->add("App", dirname(__DIR__));


require __DIR__.'/../App/app.php';

//On lance l'application
$app->run();